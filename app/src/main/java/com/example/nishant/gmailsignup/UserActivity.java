package com.example.nishant.gmailsignup;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class UserActivity extends AppCompatActivity {
    private static final String TAG ="Profile Activity" ;
    private Button buttonSignout;
    public static final int REQUEST_CODE = 100;
    public static final int PERMISSION_REQUEST = 200;
private TextView textViewusername , textViewemail ,textViewuid;
private ImageView imageView;
private  FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
private FirebaseAuth mAuth;
private FirebaseAuth.AuthStateListener mAuthListner;
private FirebaseFirestore db = FirebaseFirestore.getInstance();
private Map<String, Object> Guser = new HashMap<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        textViewusername = (TextView)findViewById(R.id.textView1);
        textViewemail =  (TextView)findViewById(R.id.textView2);
        textViewuid = (TextView)findViewById(R.id.textView3);
        imageView = (ImageView)findViewById(R.id.imageView);
        buttonSignout = (Button)findViewById(R.id.buttonSignout);


        if (user != null) {
            // Name, email address, and profile photo Url
            String name = user.getDisplayName();
            textViewusername.setText(name);
            String email = user.getEmail();
            textViewemail.setText(email);
            Uri photoUrl = user.getPhotoUrl();
            String sphotoUrl = photoUrl.toString();
           // imageView.setImageURI(photoUrl);
            new DownLoadImageTask(imageView).execute(sphotoUrl);

           // Picasso.with(context).load("http://i.imgur.com/DvpvklR.png").into(imageView);
            Log.d(TAG, "Profile photo:" + photoUrl);
           // String sphotoUrl = photoUrl.toString();
           // Bitmap bitmap=BitmapFactory.decodeStream(getContentResolver().openInputStream());

            // Check if user's email is verified
            boolean emailVerified = user.isEmailVerified();
            // The user's ID, unique to the Firebase project. Do NOT use this value to
            // authenticate with your backend server, if you have one. Use
            // FirebaseUser.getToken() instead.
            String uid = user.getUid();
            textViewuid.setText(uid);

            Guser.put("name", name);
            Guser.put("email id = ", email);
            Guser.put("UId", uid);

        }


        // Add a new document with a generated ID
        db.collection("AUthG").document("Users")
                .set(Guser)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully written!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                    }
                });
          db.collection("AUthG").document("Users")
                .update(
                        "name", "updated name posted"

                );

        buttonSignout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(UserActivity.this,MainActivity.class) );
                finish();
            }
        });

        mAuthListner = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                if(firebaseAuth.getCurrentUser()== null){
                    startActivity(new Intent(UserActivity.this,MainActivity.class) );
                    finish();
                }// if user is not logged in redirect to main

            }
        }; // Auth state listener

        // Write a message to the database


    }
}
